 @extends('layouts.patient')

@section('content')

<!-- <div class="about" style="width: 25%; height: auto">
  <div class="desc" style="background-color: lightgreen; border: 1px solid #ccc">
     requirements:
     <br>original copy of medical abstract
     <br>medical certification
     <br>Valid ID's (voter's ID, passport, etc.)
     <br>hospital bill
     <br>official quotation of breakdown of expenses
  </div>
</div>  -->

<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
</style>

<head>

    
    <link rel="stylesheet" href="/css/form-basic.css">


</head>

<br><br>

    <div class="main-content" >

        <div style="float: left;border: 1px solid #e65c00;margin-left: 20px;padding: 15px;color:#ffa366">
          <h3 style="text-align: center"><strong style="font-size: 50px">REQUIRED!</h3>
          <p style="font-size:10px;text-align: center">Failure to comply requirements may result to<br> unprocessed  application form.</p>
          <p>* Medical Abstract <br>* Medical Certificate <br> * Valid Id <br> * Hospital Bill <br>* Breakdown of Expenses</p>   
        </div>

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic" action="{{url('/singlelist')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}

            <div class="form-title-row">
                <h1>Share your Story!</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Goal Title:</span>
                    <input type="text" name="title">
                </label>

            </div>

            <div class="form-row">
                <label>
                    <span>Post Story</span>
                    <textarea name="story" rows="4" cols="60"></textarea>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Goal</span>
                    <input type="text" name="goal">
                    <p style="color: #259C9C; font-size: 10pt; margin-top: 13px; font-style: italic; font-weight: normal; text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Amount you want to raise.</p>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Add a profile goal</span>
                    <span style="padding-left: 0;"><input type="file" name="profile"></span>
                    <p style="color: #259C9C; font-size: 10pt; font-style: italic; font-weight: normal; text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Upload profile photo of your story.</p>
                </label>

            </div>

            <div class="form-row">
                <label>
                    <p style="color: #259C9C; font-size: 10pt; margin-top: 13px; font-style: italic; font-weight: normal; text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Requirements.</p>
                    <span>Medical Abstract</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalAbstract"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Medical Certification</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalCertificate"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Valid ID</span>
                    <span style="padding-left: 0;"><input type="file" name="validID"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Hospital Bill</span>
                    <span style="padding-left: 0;"><input type="file" name="hospitalBill"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Breakdown of Expenses</span>
                    <span style="padding-left: 0;"><input type="file" name="breakdownExpenses"></span>
                </label>
            </div>


            <div class="form-row">
                <label>
                    <span>Beneficiary Name</span>
                    <input type="text" name="bname">
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Illness</span>
                    <input type="text" name="illness">
                </label>
            </div>

            <div >
                <label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input style="color: white" type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>

        </form>

    </div>
    <br><br>

</body>

</html>




@endsection 