<?php 
use App\User;
$name = User::where('id',Auth::id())->get();
// echo $name;
 ?>

@extends('layouts.voucher')

 
@section('content')


<head>

    <!-- <link rel="stylesheet" href="/css/form-basic.css"> -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
    body {

        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
        h1 {
        display: inline-block;
        box-sizing: border-box;
        color:  #4c565e;
        font-size: 24px;
        padding: 0 10px 15px;
        border-bottom: 2px solid #6caee0;
        margin: 0;}
    </style>

</head>


<br><br>

        <div style="" align="center">
                <form class="form-basic" action="{{url('/voucher')}}" method="post">
            {{csrf_field()}}
        
            <div class="form-title-row">
                <h1 style="font-size: 45px;font-family: Verdana; margin-left: 50px;">Buy Vouchers<img src="/images/arrows.png" height="10%" width="15%" /></h1>
            </div><br><br>


            <div class="form-row">
                <label>
                   <!--  <p style="text-align: center;"><input type="checkbox" name="check100" value="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                    <img src="/images/v_100.JPG" height="28%" width="90%"/></p>
                    
            
            <p style="text-align: center">

           

            <input type=number min="0" name='qty100' id='qty100' style="width: 100px"  onclick="getTotal()" />
          
            </p>


                </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            

                <label>
                    <!--  <p style="text-align: center;"><input type="checkbox" name="check500" value="500">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  -->            
                    <img src="/images/v_500.JPG" height="28%" width="90%" /></p>
            
            <p style="text-align: center">
            
            <input type='number' min="0" name='qty500' id='qty500' style="width: 100px" onclick="getTotal()" />
            
            </p>

                </label>
            </div><br><br>

             <div id="totalVoucher" style="float: center;width:500px;height:100px; font-size: 20pt;"></div><br>

            <div class="form-row">
                <label>
                     <!-- <p style="text-align: center;"><input type="checkbox" name="check1000" value="1000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                     <img src="/images/v_1000.JPG" height="28%" width="90%" /></p>
            
            <p style="text-align: center">
            
            <input type='number' min="0" name='qty1000' id='qty1000' style="width: 100px" onclick="getTotal()" />
            
            </p>

                </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           

            
                <label>
                    <!-- <p style="text-align: center;"><input type="checkbox" name="check5000" value="5000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                    <img src="/images/v_5000.JPG" height="28%" width="90%" /></p>
            
            <p style="text-align: center">
            
            <input type='number' min="0" name='qty5000' id='qty5000' style="width: 100px" onclick="getTotal()" />
            
            </p>

                </label>
            </div>
<br>
            <div style="background-color:#f7e5c5 ;opacity: 0.8;padding: 5px;width: 700px">
<br>
            <div style="float: center;"><p style="color: red;font-size: 15px;background-color: #f4f4f4;width: 500px;height: 90px;text-align: center;border: 1px solid #ccc;padding: 5px"><strong>NOTE!</strong>&nbsp;For new users we would like to inform that before availing vouchers make sure that you have already transfered money to our BPI bank account provided below for fast transaction in buying vouchers. Thank You!</p></div>
    
    

            <h5 style="font-weight: bold;font-family: Arial;color:grey;font-size: 15pt"><img src="/images/lock.png" height="2%" width="2%" />BANK DETAILS</h5>
            <h4>Bank Name</h4>
            <p style="font-weight: bolder; color: grey; font-size: 25px;border: 1px solid #ccc;background-color: #F5F4F4; width: 200px">BPI</p>
            <h4>Account Name</h4>
            <p style="font-weight: bolder; color: grey; font-size: 25px;border: 1px solid #ccc;background-color: #F5F4F4; width: 200px">HelpXP</p>
            <h4>Bank Account</h4>
            <p style="font-weight: bolder; color: grey; font-size: 25px;border: 1px solid #ccc;background-color: #F5F4F4; width: 200px">10987654321</p>
<br>
               <div class="form-row">
                <label>
                    <span style="font-size: 15pt">Upload Copy of your Receipt</span>
                    <span style="padding-left: 0;font-size: 12pt"><input type="file" name="receipt"></span>
                </label>
            </div>

            @foreach($name as $user)
            <input type="hidden" name="name" value="{{$user->name}}">
            @endforeach
             
</div>         
<br>
            <div >
                <label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>
                <p style="color: grey">*By continuing you agree to HelpXP's terms and policy</p>
                <p id="total"></p>
        </form>

        </div>

</div>

@if(Session::has('error'))
    <script>
        alert('Cant buy with that quantity');
    </script>
@elseif(Session::has('success'))
    <script>
        alert('Voucher/s has been added to your account but still needs to be checked!');
    </script>
@endif

<script>
     
function getTotal() {
    var v1 = document.getElementById("qty100").value;
    var v2 = document.getElementById('qty500').value;
    var v3 = document.getElementById('qty1000').value;
    var v4 = document.getElementById('qty5000').value;

    var total = (v1 * 100) + (v2 * 500) + (v3 * 1000) + (v4 * 5000);
    document.getElementById("totalVoucher").innerHTML =  "Total Vouchers &#8369 "+ total;
}
</script>


<br><br>
@endsection



