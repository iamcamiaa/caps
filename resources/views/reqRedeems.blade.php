<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HELPXP - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
<style>
	img {
    transition:transform 0.25s ease;
	}

	img:hover {
    -webkit-transform:scale(1.5); /* or some other value */
    transform:scale(1.5);
	}

	html,
body {
  height: 100%;
}

.container {
  display: table;
  width: 100%;
  height: 100%;
}

.interior {
  display: table-cell;
  vertical-align: middle;
  text-align: center;
}

.btn {
  background-color: #fff;
  padding: 1em 3em;
  border-radius: 3px;
  text-decoration: none;
}

.modal-window {
  position: fixed;
  background-color: rgba(255, 255, 255, 0.15);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  opacity: 0;
  pointer-events: none;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}

.modal-window:target {
  opacity: 1;
  pointer-events: auto;
}

.modal-window>div {
  width: 400px;
  position: relative;
  margin: 10% auto;
  padding: 2rem;
  background: rgba(139, 130, 125, 0.54);
  color: #444;
}

.modal-window header {
  font-weight: bold;
}

.modal-close {
  color: #aaa;
  line-height: 50px;
  font-size: 80%;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  width: 70px;
  text-decoration: none;
}

.modal-close:hover {
  color: #000;
}

</style>

</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Help</span>XP</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->username }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<!-- <form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="active"><a href="{{ url('/displaypatients') }}"><em class="fa fa-calendar">&nbsp;</em> Patients</a></li>
			<li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-bar-chart">&nbsp;</em> Sponsors</a></li>
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Patients</h1>
			</div>
		</div><!--/.row-->


	<div class="panel panel-container">
	<div class="row">

		<form action="{{ url('/filterPatient') }}" method="post">
	{{csrf_field()}}
	<center>From: <input type="date" name="from" id="from">
	To: <input type="date" name="to" id="to">
	<input type="submit" name="filter" value="filter"></center>
	<!-- <a href="{{ url('/filter') }}" class="btn btn-info" name="filter" id="filter">Filter</a></p> -->
	</form>

	<table class="table">

	<th class="fixed-table-container">Profile</th>
	<th class="fixed-table-container">Story Title</th>
	<th class="fixed-table-container">User</th>
	<th class="fixed-table-container">Patient Name</th>
	<th class="fixed-table-container">Amount Goal</th>
	<th class="fixed-table-container">Raised Funds</th>
	<th class="fixed-table-container">Ongoing Amount</th>
	<th class="fixed-table-container">RedeemStatus</th>
	<th class="fixed-table-container">Action</th>
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>



<p id="patient">
	@foreach ($redeem as $rdm)
	@if($rdm->patient->redeemStatus != "For release")
		<tr class="fixed-table-container">
			<td class="fixed-table-container"><img src="{{  url('storage/picture/'.$rdm->patient->filename) }}" width="100px" height="100px" /></td>
			<td class="fixed-table-container">{{$rdm->storytitle}}</td>
			<td class="fixed-table-container">{{$rdm->patient->userName->name}}</td>
			<td class="fixed-table-container">{{$rdm->patient->patientname}}</td>
			<td class="fixed-table-container">{{$rdm->patient->goal}}</td>
			<td class="fixed-table-container">{{$rdm->patient->TotalRedeem}}</td>
			<td class="fixed-table-container">{{$rdm->patient->newgoal}}</td>
			<td class="fixed-table-container">{{$rdm->patient->status}}</td>
	<!-- <form action="{{url('/requests')}}" method="POST">
		{{csrf_field()}} -->
			<td class="fixed-table-container" align="center">
				<div class="container">
  				<div class="interior">
    			<a class="btn" href="#{{$rdm->patient->patientid}}">Release</a>
  				</div>
				</div>

				<div id="{{$rdm->patient->patientid}}" class="modal-window">
  				<div>
    		<a href="#modal-close" title="Close" class="modal-close">Close</a>
    		<div class="main-content" >

        <form class="form-basic" action="{{url('/requests')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}

        <input type="hidden" name="patientid" value="{{$rdm->patient->patientid}}">

            <div class="form-title-row">
                <h2><strong>Voucher Details!</strong></h2>
            </div>
            <table border="0" cellpadding="20px">
            <tr>
            <div class="form-row">
                <label>
                    <td><strong>Date</strong></td>
                   <td> <input type="text" name="title" value="{{$rdm->patient->updated_at->format('F d, Y')}}"></td>
                </label>
             </tr>
            </div>

 			<tr>
                  <td><strong>Beneficiary Name</strong></td> 
                   <td> <input type="text" name="title" value="{{$rdm->patient->patientname}}"></td>
                
			</tr>
            
			<tr>
                <label>
                    <td><strong>Amount Redeemed</strong></td>
                    <td><input type="text" name="title" value="{{$rdm->patient->redeemed}}"></td>
                </label>
             </tr>
            </table><br><br>
            <div>
                <label>
                    <span><input type="submit" class="btn btn-primary" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>

        </form>

    </div>
				</div>
			</div>
			</td>
			<!--  -->
	<!-- </form> -->

		</tr>
@endif
@endforeach

</p>
</table>
				
	
				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->

</body>
</html>

<script>
function updateState(context){
     context.setAttribute('disabled',true)
    }
</script>

	
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		




