<?php 

Route::get('/', 'UserController@sharedStories');

Route::get('/about', function(){
	return view('about');
});

Route::view('/adminlogin', 'Auth.admin-login');
Route::post('/verifyadmin', 'AdminHomeController@adminLogin');

Auth::routes();
Route::get('users/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');


Route::post('/singlelist', 'PatientController@savePatient');
Route::get('/list/{id}/view','PatientController@patient');	
Route::get('/donors/{id}/view','PatientController@donors');
Route::get('/update/{id}/view','PatientController@updatepage');
route::middleware(['auth'])->group(function(){

Route::get('/getDen','SponsorController@getDen');

// Route::post('/notification/get', 'NotificationController@get');

Route::get('/home', 'PatientController@displayPatient');
Route::get('/patientsdetail', 'PatientController@newPatient');



Route::get('/history', 'UserController@history');
Route::get('/donateSponsor', 'SponsorController@displaySponsorHistory');
Route::get('/sponsorDonate/{patientid}', 'SponsorController@newSponsor');
Route::get('/donateAny/{sponsorid}','SponsorController@newSponsorAny');
Route::post('/helpxp', 'SponsorController@saveSponsor');
Route::post('/homepage', 'SponsorController@saveSponsorAny');
Route::get('/confirm/{patientid}', 'PatientController@voucher');
Route::post('/redeem', 'PatientController@redeem');

Route::get('/total', 'UserController@total');

Route::get('/buyvoucher/{userid}', 'SponsorController@buyvoucher');
Route::post('/voucher', 'SponsorController@savevoucher');
Route::get('/update/{patientid}', 'PatientController@updateStory');
Route::post('/saveUpdate', 'PatientController@saveupdateStory');
//ADMIN Routes
Route::get('/approve', 'AdminHomeController@approveStories');
Route::post('/approved', 'AdminHomeController@approved');
Route::get('/request', 'AdminHomeController@requestRedeem');
Route::post('/requests', 'AdminHomeController@requestApproveRedeem');
Route::get('/displaypatients', 'AdminHomeController@viewPatients');
Route::get('/displaysponsors', 'AdminHomeController@viewSponsors');
Route::get('/delete', 'AdminHomeController@deleteUsers');
Route::get('/patientsponsor/{userid}', 'AdminHomeController@patientSponsor');
Route::get('/sponsorsponsored/{userid}', 'AdminHomeController@sponsorSponsored');
Route::get('/patienthistory/{userid}', 'AdminHomeController@patienthistory');
Route::get('/sponsorhistory/{userid}', 'AdminHomeController@sponsorhistory');
Route::get('/displayusers', 'AdminHomeController@viewUsers');
Route::post('/filter', 'AdminHomeController@filterSponsorDate');
Route::post('/filterPatient', 'AdminHomeController@filterPatientDate'); //not working


//angel
Route::get('/check', 'AdminHomeController@checkPayment');
Route::post('/checked', 'AdminHomeController@checked');
Route::get('/viewvoucher', 'SponsorController@getValue');



});


