<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    //S
    protected $primaryKey = 'picid';
    public function patient(){
    	return $this->hasOne('App\Patient');
    }

    public function stories(){
    	return $this->hasOne('App\Stories', 'storyid', 'storyid');
    }

}
