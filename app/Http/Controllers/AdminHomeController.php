<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests;

use Auth;
use App\User;
use App\Stories;
use App\Picture;
use App\Patient;
use App\Sponsor;
use App\Donation;
use App\Billing;
use Carbon\Carbon;
use DB;


class AdminHomeController extends Controller
{
    public function adminLogin(Request $req) {
        //return $req;
        if (Auth::attempt(['username' => $req->username, 'password' => $req->password, 'role' => 'admin']))
        {

            return view('displayUsers');
        }
        else
            return view('auth.admin-login');
    }

    public function viewUsers()
    {
        if(Auth::user()->role == "admin"){
         $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->get();
        $success = Patient::whereRaw('goal = TotalRedeem')->get();
            return view('displayUsers')->with(['data'=>$data, 'success'=>$success]);
 
         

        }
        else
            return "ERROR!";

    }

    // public function deleteUsers(Request $request)
    // {
    //    $user = User::findOrFail($request->id);
    //    $user->delete();
    //    return redirect(url('/displayusers'));
    // }

    public function viewPatients()
    {
        if(Auth::user()->role == "admin"){
            $users = Patient::select('userid')->distinct('userid')->get();
            return view('displaypatients', compact('users'));
    }
        else
            return "ERROR!";
    }

    public function viewSponsors()
    {
        if(Auth::user()->role == "admin"){
            $users = Sponsor::select('userid')->distinct('userid')->get();
            return view('displaysponsors', compact('users'));
            }
        else
            return "ERROR!";
    }
    

    public function patientSponsor($userid) {

       //   $id = Donation::where('patientid', '=', $userid)->get();
       // return view('displayPatientSponsor')->with([ 'patientid' => $id]);

        if(Auth::user()->role == "admin"){
        $patient = Patient::where('userid', $userid)->get();
        $voucher = Donation::get();
        $patientCollect = new Collection();
        foreach($patient as $pts) {
            foreach ($voucher as $vcr) {
                if($pts['patientid'] == $vcr['patientid']) {
                    $patientCollect->push($vcr);
                 
                }
            }
            
        }
      
       return view('displayPatientSponsor')->with([ 'patientCollect' => $patientCollect]);
    }
    else
        return "ERROR!";

    }

    public function sponsorSponsored($userid) {
        if(Auth::user()->role == "admin"){
        $sponsor = Sponsor::where('userid', $userid)->get();
        $voucher = Donation::get();
        $sponsorCollect = new Collection();
        foreach($sponsor as $spr){
            foreach($voucher as $dnr){
                if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
                $sponsorCollect->push($spr);
                }
            }
        }

        return view('displaySponsorSponsored')->with(['sponsorCollect'=>$sponsorCollect]);
    }

    else return "ERROR!";
    }

    public function patienthistory($userid) {
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $patients = Patient::where('userid', "=" ,$userid)->get();
        return view('patienthistory')->with(['user'=>$patients]);
    }
        else 
            return "ERROR";
    }

    public function sponsorhistory($userid) {
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $sponsors = Sponsor::where('userid', "=" ,$userid)->get();
        return view('sponsorhistory')->with(['user'=>$sponsors]);
         }
        else 
            return "ERROR";
    }

    public function approveStories() {
        if(Auth::user()->role == "admin"){
            $patient = Patient::where('status', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }
            return view('approveStories')->with(['approve'=>$approve]);
        }
        else 
            return "ERROR";

    }

    public function approved(Request $request) {
        if(Auth::user()->role == "admin"){
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->status = "approved";
            $pnt->save();

            $patient = Patient::where('status', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }
            return view('approveStories')->with(['approve'=>$approve]);
        }

        else 
            return "ERROR";

    }

    

    public function filterSponsorDate(Request $request){ 
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = sponsor::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displaySponsors')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

    public function requestRedeem()
    {
        if(Auth::user()->role == "admin") {
         $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);
            }

        $user = Auth::id();    
        $released = Patient::where('userid', $user)->where('status', 'partial')->orWhere('status', 'full');
        // $str = Stories::get();
        // $released = new Collection();
        // foreach($pnt as $pnt) {
        //     foreach($str as $str){
        //         if($pnt['storyid'] == $str['storyid']){
        //         $released->push($pnt);
        //         }
        //     }
        // }

            return view('reqRedeems')->with(['redeem'=>$redeem, 'released'=>$released]);
        }
        else
            "ERROR";
    }


    public function requestApproveRedeem(Request $request)
    {
        if(Auth::user()->role == "admin"){
        $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->redeemStatus = "For release";
            $pnt->save();
            
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);
            }

            return view('reqApproveRedeems')->with(['redeem'=>$redeem]);
            }

        
       
        else
            return "ERROR!";

    }

    public function filterPatientDate(Request $request){ 
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = patient::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displayPatients')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

        // angel
    public function checkPayment() {
        if(Auth::user()->role == "admin"){
            $bill = Billing::where('ifReceived','=','pending')->get();
            $check = new Collection();
            foreach($bill as $bills){
                $candonate = $bills->sponsors[0];
                $check->push($candonate);
            }
            return view('check')->with(['bill'=>$bill]);
        }
        else 
            return "ERROR";

    }

    public function checked(Request $request) {
        if(Auth::user()->role == "admin"){
            $bill = Billing::find($request->billing_id);
            $bill->ifReceived = "received";
            $bill->save();


            $find = Sponsor::findOrFail($request->userid);
            $sponsor = Sponsor::where('billing_id','=',$request->billing_id)->get();
            foreach ($sponsor as $sponsors) {
              $sponsors->status = null;
              $sponsors->save();
            }
            

            $sponsor = Sponsor::where('status', null)->get();
            $approve = new Collection();
            foreach($sponsor as $sponsors){
                $candonate = $sponsors->sponsors[0];
                $approve->push($candonate);
            }
            return view('checkedPayment')->with(['sponsor'=>$sponsor]);
        }

        else 
            return "ERROR";

    }




}
