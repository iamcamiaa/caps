<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sponsor;
use App\User;
use App\Donation;
use App\Patient;
use App\Stories;
use App\Picture;
use DB;

class UserController extends Controller
{
    public function history(){
 


//current story
        $user = Auth::id();
        
        $patientDetails = Patient::where('userid', $user)->where('status', 'approved')->get();
//sponsors                      
$sponsor = Sponsor::where('userid', $user)->get();
$donation = Donation::get();
$sponsorCollect = new Collection();
foreach($sponsor as $spr){
    foreach($donation as $dnr){
        if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
            $sponsorCollect->push($spr);
            }
        }
}

//redeem
       
        $redeemdetails = Patient::where('userid', $user)->where('status', 'redeem')->get();

        return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'patientDetails'=>$patientDetails, 'redeemdetails'=>$redeemdetails]);
    }




    public function total(Request $request){
         $user = Auth::id();

        $patient = Patient::where('userid', $user)->where('status', 'approved')->get();
        // $data = new Collection();
        // foreach($patient as $pnt){
        //     //$d['total'] = $pnt->TotalRedeem;
        //     $data->push($pnt);
        // }
        return $patient;
    }



    

    public function sharedStories(Request $request){
        $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->get();
        return view('welcome')->with(['data'=>$data]);
    }

    
}
 